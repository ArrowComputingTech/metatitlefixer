#Fixes metadata for The Score - renames newest 2 track titles to 'Hour 1' and 'Hour 2'.

import os, glob, eyed3, sys

SHOW_PATH='/pickapath/'
os.chdir(SHOW_PATH)

def isMP3(f):
    if (str(f).lower()).endswith('mp3'):
        return True
    return False

def fixMeta(new_files):
    for i, track in enumerate(new_files):
        track_stinfo = os.stat(track)
        audio_file = eyed3.load(track)
        print ('ID3 title tag for ' + track + ' before: ' + audio_file.tag.title)
        audio_file.tag.title = 'Hour ' + str(i + 1)
        audio_file.tag.save()
        os.utime(track, (track_stinfo.st_atime, track_stinfo.st_mtime))
        print ('ID3 title tag for ' + track + ' after: ' + audio_file.tag.title)

# Sort files by modified time, append newest 2 mp3 files to newest_files.
def arrangeFiles(file_list):
    newest_files = []
    file_list.sort(key=os.path.getmtime)
    i=0
    while len(newest_files) < 2:
        if i > 20:
            print ('Error in program: no mp3s detected. Terminating.')
            raise SystemExit
        if isMP3(file_list[i]):
            newest_files.append(file_list[i])
        i+=1
    return (newest_files)

def orchestrate():
    print ("Setting metadata for newest episode of 'The Score'...")
    file_list = glob.glob('./*')
    newest_files = arrangeFiles(file_list)
    fixMeta(newest_files)

orchestrate()
